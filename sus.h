#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#include <iostream>
#include <windows.h>
#include <string>
#include <winsock2.h>
#include <Ws2tcpip.h>
#include <winuser.h>
#pragma comment(lib, "User32.lib")
#pragma comment(lib, "Ws2_32.lib")

using namespace std; 

//XOR data with provided key 
void XOR(char* data, size_t data_len, char* key, size_t key_len) {
    int keyIter = 0;

    for(int i = 0; i < data_len; i++) {
        if(keyIter == key_len - 1){
            keyIter = 0;
        }
        data[i] = data[i] ^ key[keyIter];
        keyIter++;
    }
}